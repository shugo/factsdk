﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactSDK.Server
{
    public class User
    {
        Fact.Network.Client _Client = null;

        public User(Fact.Network.Client Client)
        {
            _Client = Client;
        }

        public string Firstname
        {
            get 
            {
                object result = _Client.Call("UserService.GetUserFirstname");
                if (result == null || !(result is string))
                {
                    throw new Exception("Transaction rejected by the server");
                }
                return (result as string).Trim();
            }
            set
            {
                object result = _Client.Call("UserService.SetUserFirstname", value.Trim());
                if (result == null || !(result is bool) || !((bool)result))
                {
                    throw new Exception("Transaction rejected by the server");
                }
            }
        }
        public string Lastname
        {
            get
            {
                object result = _Client.Call("UserService.GetUserLastname");
                if (result == null || !(result is string))
                {
                    throw new Exception("Transaction rejected by the server");
                }
                return (result as string).Trim();
            }
            set
            {
                object result = _Client.Call("UserService.SetUserLastname", value.Trim());
                if (result == null || !(result is bool) || !((bool)result))
                {
                    throw new Exception("Transaction rejected by the server");
                }
            }
        }
        public string Username
        {
            get
            {
                object result = _Client.Call("UserService.GetUsername");
                if (result == null || !(result is string))
                {
                    throw new Exception("Transaction rejected by the server");
                }
                return (result as string).Trim();
            }
        }
    }
}
