﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactSDK.Server
{
    public class RemoteTask
    {
        int ID = -1;
        Fact.Network.Client _Client = null;
        bool _Published = false;
        public bool Valid
        {
            get
            {
                return ID >= 0 && _Client != null;
            }
        }
        public bool Finished
        {
            get
            {
                if (_Client == null) { return true; }
                if (ID == -1) { return true; }
                try
                {
                    if (!_Published) { return false; }
                    return (bool)_Client.Call("AgentManager.IsJobFinished", ID);
                }
                catch { return true; }
            }
        }

        public string StdOut
        {
            get
            {
                if (_Client == null) { return ""; }
                if (ID == -1) { return ""; }
                try
                {
                    if (!_Published) { return ""; }
                    string result = (string)_Client.Call("AgentManager.GetJobStdOutLog", ID);
                    if (result != null) { return result; }
                }
                catch { }
                return "";
            }
        }

        public string StdErr
        {
            get
            {
                if (_Client == null) { return ""; }
                if (ID == -1) { return ""; }
                try
                {
                    if (!_Published) { return ""; }
                    string result = (string)_Client.Call("AgentManager.GetJobStdErrLog", ID);
                    if (result != null) { return result; }
                }
                catch { }
                return "";
            }
        }

        public RemoteTask(Fact.Network.Client Client, FactSDK.Server.Agents.Agent Agent)
        {
            _Client = Client;
            try
            {
                ID = (int)Client.Call("AgentManager.CreateJobOnAgent", Agent._Id);
            }
            catch { ID = -1; }
        }

        public RemoteTask(Fact.Network.Client Client, string Group)
        {
            _Client = Client;
            try
            {
                ID = (int)Client.Call("AgentManager.CreateJob", Group);
            }
            catch { ID = -1; }
        }

        public void Publish()
        {
            if (ID < 0) { return; }
            if (_Client == null) { return; }
            if (_Published) { return; }
            _Published = true;
            try
            {
                if (!(bool)_Client.Call("AgentManager.PublishJob", ID)) { ID = -1; }
            }
            catch { ID = -1; }
        }

        public void SendFile(string File, string Path)
        {
            if (ID < 0) { return; }
            if (_Client == null) { return; }
            if (_Published) { return; }
            if (!System.IO.File.Exists(File)) { return; }
            try
            {
                SendFile(new Fact.Processing.File(System.IO.File.ReadAllBytes(File), System.IO.Path.GetFileName(Path)), Path);
            }
            catch { ID = -1; }
        }

        public void SendFile(Fact.Processing.File File, string Path)
        {
            if (ID < 0) { return; }
            if (_Client == null) { return; }
            if (_Published) { return; }

            while (Path.StartsWith("./")) { Path = Path.Substring("./".Length); }

            try
            {
                bool status = false;
                status = (bool)(_Client.Call("AgentManager.AddFileInJobLocalStorageFromCache", ID, File.ContentHash, Path));
                if (!status)
                {
                    status = (bool)(_Client.CallWithTimeout("AgentManager.AddFileInJobLocalStorage", 60000, ID, File, Path));
                    if (status == false) { ID = -1; return; }
                }
                status = (bool)(_Client.Call("AgentManager.PushAction_CopyFileFromJobLocalStorageToAgent", ID, "./" + Path, Path));
                if (status == false) { ID = -1; return; }
            }
            catch { ID = -1; }
        }

        public void Run(string Command, params string[] args)
        {
            if (ID < 0) { return; }
            if (_Client == null) { return; }
            if (_Published) { return; }
            try
            {
                bool status = false;
                status = (bool)(_Client.Call("AgentManager.PushAction_RunCommandOnAgent", ID, Command, args));
                if (status == false) { ID = -1; return; }
            }
            catch { ID = -1; }

        }

        public void ExportFile(string Path)
        {
            if (ID < 0) { return; }
            if (_Client == null) { return; }
            if (_Published) { return; }

            while (Path.StartsWith("./")) { Path = Path.Substring("./".Length); }

            try
            {
                bool status = false;
                status = (bool)(_Client.Call("AgentManager.PushAction_CopyFileFromAgentToJobLocalStorage", ID, "./" + Path, Path));
                if (status == false) { ID = -1; return; }
            }
            catch { ID = -1; }
        }


        public void DeleteFile(string Path)
        {
            if (ID < 0) { return; }
            if (_Client == null) { return; }
            if (_Published) { return; }

            while (Path.StartsWith("./")) { Path = Path.Substring("./".Length); }

            try
            {
                bool status = false;
                status = (bool)(_Client.Call("AgentManager.PushAction_DeleteFileFromJobLocalStorage", ID, Path));
                if (status == false) { ID = -1; return; }
            }
            catch { ID = -1; }
        }
        public Fact.Processing.File GetFile(string Path)
        {
            if (ID < 0) { return null; }
            if (_Client == null) { return null; }
            if (!_Published) { return null; }
            try
            {
                return _Client.CallWithTimeout("AgentManager.GetFileInJobLocalStorage", 60000, ID, "output.ff") as Fact.Processing.File;
            }
            catch { ID = -1; return null; }
        }

        public void Delete()
        {
            if (ID < 0) { return; }
            if (_Client == null) { return; }
            if (!_Published) { return; }
            try
            {
                _Client.Call("AgentManager.DeleteJob", ID);
            }
            catch { ID = -1; return; }
        }

        public void Execute()
        {
            Publish();
            while (!Finished) { System.Threading.Thread.Sleep(50); }
        }
    }
}
