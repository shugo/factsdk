﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactSDK.Server
{
    public class Administration
    {
        Fact.Network.Client _Client = null;

        public Administration(Fact.Network.Client Client)
        {
            _Client = Client;
        }

        public void CreateUser(string Username, string Password)
        {
            Username = Username.Trim();
            Password = Password.Trim();
            if (Username == "" || Password == "") { throw new Exception("Invalid Username or Password"); }
            object result = _Client.Call("AdminService.CreateUser", Username, Password);
            if (result == null || !(result is bool) || !((bool)result))
            {
                throw new Exception("Transaction rejected by the server");
            }
        }

        public void DeleteUser(string Username)
        {
            Username = Username.Trim();
            if (Username == "") { throw new Exception("Invalid Username"); }
            object result = _Client.Call("AdminService.DeleteUser", Username);
            if (result == null || !(result is bool) || !((bool)result))
            {
                throw new Exception("Transaction rejected by the server");
            }
        }

        public void LoadService(string ServicePath)
        {
            if (!System.IO.File.Exists(ServicePath)) { throw new System.IO.FileNotFoundException(ServicePath); }
            Fact.Processing.File file = null;
            try
            {
                file = new Fact.Processing.File(System.IO.File.ReadAllBytes(ServicePath), System.IO.Path.GetFileName(ServicePath));
            }
            catch { throw new Exception("Impossible to load the service file"); }
            object result = _Client.Call("AdminService.LoadServiceFromFile", file);
            if (result == null || !(result is bool) || !((bool)result))
            {
                throw new Exception("Transaction rejected by the server");
            }
        }

        public Fact.Directory.Directory Directory
        {
            get
            {
                object result = _Client.Call("AdminService.GetServerDirectoryStripped");
                if (result == null || !(result is Fact.Directory.Directory))
                {
                    throw new Exception("Transaction rejected by the server");
                }
                return result as Fact.Directory.Directory;
            }
        }
    }
}
