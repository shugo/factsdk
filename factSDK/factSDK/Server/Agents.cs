﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactSDK.Server
{
    public class Agents
    {
        public class Agent
        {
            internal Agents _Parent = null;
            string _Group = "";
            internal string _Id = "";
            public string ID
            {
                get
                {
                    return _Id;
                }
            }
            public string Name
            {
                get
                {
                    try
                    {
                        return _Parent._Client.Call("AgentManager.GetAgentName", _Id) as string;
                    }
                    catch
                    { throw new Exception("Server request failed"); }
                }
            }
            internal Agent() { }
        }

        Fact.Network.Client _Client = null;
        public Agents(Fact.Network.Client Client)
        {
            _Client = Client;
        }

        string[] _ListAgents()
        {
            return _Client.Call("AgentManager.GetAgentList") as string[];
        }

        List<Agent> ConnectedAgents
        {
            get
            {
                string[] list = _ListAgents();
                List<Agent> agents = new List<Agent>();
                foreach (string id in list)
                {
                    agents.Add(new Agent() { _Id = id, _Parent = this });
                }
                return agents;
            }
        }

    }
}
