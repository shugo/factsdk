﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactSDK.Server
{
    public class Challenge
    {
        static Dictionary<string, string> _CacheTeamName = new Dictionary<string, string>();
        static Dictionary<string, string> _CachePlayerName = new Dictionary<string, string>();
        static Dictionary<string, string> _CacheLobbyName = new Dictionary<string, string>();

        public static Lobby[] GetLobbies(Fact.Network.Client Client)
        {
            List<Lobby> Lobbies = new List<Lobby>();
            string[] tokens = Client.Call("Challenge.GetLobbies") as string[];
            if (tokens != null)
            {
                foreach (string token in tokens)
                {
                    Lobby lobby = new Lobby();
                    lobby._Token = token;
                    lobby._Client = Client;
                    string name = "";
                    lock (_CacheLobbyName)
                    {
                        if (!_CacheLobbyName.ContainsKey(token))
                        {
                            try
                            {
                                name = Client.Call("Challenge.GetLobbyName", token) as string;
                                if (name != null && name != "") { _CacheLobbyName.Add(token, name); }
                            }
                            catch { }
                        }
                        else
                        {
                            name = _CacheLobbyName[token];
                        }
                    }
                    lobby._Name = name;
                    if (name != null && name != "") { Lobbies.Add(lobby); }
                }
            }
            return Lobbies.ToArray();
        }

        public class Lobby
        {
            internal Fact.Network.Client _Client = null;
            internal string _Name = "";
            internal string _Token = "";
            internal Lobby() { }

            public Lobby(Fact.Network.Client Client, string Name)
            {
                _Client = Client;
                _Name = Name;
                _SetupLobby();
            }

            bool _Setup = false;

            void _SetupLobby()
            {
                if (!_Setup)
                {
                    _Token = _Client.Call("Challenge.CreateLobby", _Name) as string;
                    if (_Token == null || _Token.Trim() == "")
                    {
                        throw new Exception("Impossible to create the lobby");
                    }
                    _Setup = true;
                }
            }

            public bool Open
            {
                get
                {
                    _SetupLobby();
                    throw new NotImplementedException();
                }
                set
                {
                    _SetupLobby();
                    if (value)
                    {
                        if (!(bool)_Client.Call("Challenge.OpenLobby", _Token))
                        {
                            throw new Exception("Impossible to open the lobby");
                        }
                    }
                    else
                    {
                        if (!(bool)_Client.Call("Challenge.CloseLobby", _Token))
                        {
                            throw new Exception("Impossible to close the lobby");
                        }
                    }
                }
            }

            public string Name
            {
                get { return _Name; }
            }

            public void Delete()
            {
                if (!_Setup)
                {
                    string token = _Token;
                    lock (this)
                    {
                        _Token = "";
                    }
                    try
                    {
                        _Client.Call("Challenge.DeleteLobby", token);
                    }
                    catch { }
                }
            }

            public bool Kick(Team Team)
            {
                return (bool)_Client.Call("Challenge.KickTeamFromLobby", _Token, Team._Token);
            }

            public List<Team> Teams
            {
                get
                {
                    List<Team> teams = new List<Team>();
                    string[] teamsTokens = null;
                    teamsTokens = _Client.Call("Challenge.GetTeamsInLobby", _Token) as string[];
                    if (teamsTokens == null) { throw new Exception("Impossible to list the teams in the lobby"); }
                    foreach (string token in teamsTokens)
                    {
                        teams.Add(new Team(_Client, token, true));
                    }
                    return teams;
                }
            }

            public Match CreateMatch(params Team[] Teams)
            {
                List<string> teamnames = new List<string>();
                foreach (Team t in Teams)
                {
                    teamnames.Add(t._Token);
                }
                return CreateMatchFromTeamTokens(teamnames.ToArray());
            }

            private Match CreateMatchFromTeamTokens(params string[] TeamTokens)
            {
                string matchToken = _Client.Call("Challenge.CreateMatch", _Token, TeamTokens) as string;
                if (matchToken == null || matchToken.Trim() == "")
                {
                    throw new Exception("Impossible to create the team on the server");
                }
                Match match = new Match(matchToken);
                match._Client = _Client;
                return match;
            }
        }

        public class Registration
        {
            internal string _Token = "";
            internal Fact.Network.Client _Client = null;
            internal Registration() { }
            public void CancelRegistration(string Lobby)
            {
                try
                {
                    _Client.Call("Challenge.CancelTeamRegistration", _Token);
                }
                catch { }
                _Token = "";
            }
        }

        public class Team
        {
            internal Fact.Network.Client _Client = null;
            string _Name = "";
            public string Name { get { return _Name; } }
            internal string _Token = "";
            internal bool _Public = false;
            internal Team(Fact.Network.Client Client, string Token, bool Public)
            {
                _Client = Client;
                _Token = Token;
                _Public = Public;
                lock (_CacheTeamName)
                {
                    if (!_CacheTeamName.ContainsKey(_Token))
                    {
                        _Name = _Client.Call("Challenge.GetTeamName", _Token) as string;
                        if (_Name != null && _Name != "") { _CacheTeamName.Add(_Token, _Name); }
                    }
                    else
                    {
                        _Name = _CacheTeamName[_Token];
                    }
                }
            }
            public Team(Fact.Network.Client Client, string Name)
            {
                _Client = Client;
                _Name = Name;
                _Token = _Client.Call("Challenge.CreateTeam", _Name) as string;
                if (_Token == null || _Token.Trim() == "")
                {
                    throw new Exception("Impossible to create the match on the server");
                }
            }

            public Registration Register(Lobby Lobby)
            {
                if (_Public) { throw new Exception("Only the owner of the team can do this action"); }
                lock (this)
                {
                    string registrationToken = _Client.Call("Challenge.RegisterTeam", _Token, Lobby._Token) as string;
                    if (registrationToken == null || registrationToken.Trim() == "")
                    {
                        throw new Exception("Impossible to join the lobby");
                    }
                    Registration registration = new Registration();
                    registration._Token = registrationToken;
                    registration._Client = _Client;
                    return registration;
                }
            }

            public List<Match> Matches
            {
                get
                {
                    List<Match> matches = new List<Match>();
                    string[] matchtoken = null;
                    matchtoken = _Client.Call("Challenge.GetMatchesForTeam", _Token) as string[];
                    if (matchtoken == null) { throw new Exception("Impossible to list the matches for the team"); }
                    foreach (string token in matchtoken)
                    {
                        Match match = new Match(token);
                        match._Client = _Client;
                        matches.Add(match);
                    }
                    return matches;
                }
            }

            public List<Player> Players
            {
                get
                {
                    List<Player> players = new List<Player>();
                    string[] playersTokens = null;
                    playersTokens = _Client.Call("Challenge.GetPlayersInTeam", _Token) as string[];
                    if (playersTokens == null) { throw new Exception("Impossible to list the players in the team"); }
                    foreach (string player in playersTokens)
                    {
                        Player p = new Player();
                        p._Client = _Client;
                        p._Public = true;
                        p._Token = player;
                        lock (_CachePlayerName)
                        {
                            if (!_CachePlayerName.ContainsKey(player))
                            {
                                try
                                {
                                    p._Name = _Client.Call("Challenge.GetPlayerName", player) as string;
                                    if (p.Name != null && p.Name != "") { _CachePlayerName.Add(player, p._Name); }
                                }
                                catch { throw new Exception("Impossible to get the name of the player"); }
                            }
                            else
                            {
                                p._Name = _CachePlayerName[player];
                            }
                        }
                        players.Add(p);
                    }
                    return players;
                }
            }
        }

        public class Match
        {
            internal Fact.Network.Client _Client = null;
            string _Token = "";

            internal Match(string MatchToken)
            {
                _Token = MatchToken;
            }

            public void EnqueueMessageFromPlayer(Player Player, byte[] Data)
            {
                if (!(bool)_Client.Call("Challenge.EnqueueMessageFromPlayer", _Token, Player._Token, Data))
                {
                    throw new Exception("Impossible to send the message");
                }
            }

            public void EnqueueMessageToPlayer(Player Player, byte[] Data)
            {
                if (!(bool)_Client.Call("Challenge.EnqueueMessageToPlayer", _Token, Player._Token, Data))
                {
                    throw new Exception("Impossible to send the message");
                }
            }

            public void DequeueMessageFromPlayer(out Player Player, out byte[] Data)
            {
                Player = null;
                Data = null;
                object[] array = _Client.Call("Challenge.DequeueMessageFromPlayer", _Token) as object[];
                if (array == null || array.Length != 2)
                {
                    throw new Exception("Impossible to get the message");
                }
                if (array[0] == null && array[1] == null)
                {
                    return;
                }
                if (array.Length != 2 || !(array[0] is string) || !(array[1] is byte[]))
                {
                    throw new Exception("Impossible to get the message");
                }
                string token = array[0] as string;
                if (token == null) { throw new Exception("Impossible to get the message"); }
                foreach (Team team in Teams)
                {
                    foreach (Player p in team.Players)
                    {
                        if (p._Token.ToString() == token) { Player = p; goto end; }
                    }
                }
                end:
                Data = array[1] as byte[];
                if (Data == null) { Player = null; }
                if (Player == null) { Data = null; }
            }

            public void DequeueMessageToPlayer(Player Player, out byte[] Data)
            {
                Data = null;
                Data = _Client.Call("Challenge.DequeueMessageToPlayer", _Token, Player._Token) as byte[];
            }

            public List<Team> Teams
            {
                get
                {
                    List<Team> teams = new List<Team>();
                    string[] teamsTokens = null;
                    teamsTokens = _Client.Call("Challenge.GetTeamsInMatch", _Token) as string[];
                    if (teamsTokens == null) { throw new Exception("Impossible to list the teams in the match"); }
                    foreach (string token in teamsTokens)
                    {
                        teams.Add(new Team(_Client, token, true));
                    }
                    return teams;
                }
            }
        }

        public class Player
        {
            internal Fact.Network.Client _Client = null;
            internal string _Token = "";
            internal bool _Public = false;
            internal string _Name = "";
            public string Name { get { return _Name; } }
            internal Player() { }
            public Player(Fact.Network.Client Client, string PlayerName, Team Team) : this(Client, PlayerName, Team._Token) { }

            Player(Fact.Network.Client Client, string PlayerName, string TeamToken)
            {
                _Name = PlayerName;
                _Client = Client;
                _Token = _Client.Call("Challenge.JoinTeam", PlayerName, TeamToken) as string;
                if (_Token == null || _Token.Trim() == "")
                {
                    throw new Exception("Impossible to join the team");
                }
            }
        }
    }
}
