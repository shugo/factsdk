﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactSDK.Server
{
    public class Buildfarm
    {
       Fact.Network.Client _Client = null;
       
       public class Version
       {
           internal Buildfarm _Parent = null;
           internal Project _Project = null;

           internal string _Name = "";
           public string Name { get { return _Name; } }
           public void Submit(string BranchName, Fact.Processing.Project Project)
           {
               _Project.Submit(BranchName, _Name, Project);
           }
       }

       public class Branch
       {
           internal Buildfarm _Parent = null;
           internal Project _Project = null;

           internal string _Name = "";
           public string Name { get { return _Name; } }
           public void Submit(string VersionName, Fact.Processing.Project Project)
           {
               _Project.Submit(VersionName, _Name, Project);
           }
       }

       public class Submission
       {
           internal Buildfarm _Parent = null;

           internal string _ID = "";
           public string ID { get { return _ID; } }
           public Fact.Processing.Project Project
           {
               get { return null; }
           }
       }

       public class Project
       {
           internal Buildfarm _Parent = null;
           internal string _Name = "";
           public string Name { get { return _Name; } }
           public void CreateVersion(string VersionName) { _Parent.CreateProjectVersion(_Name, VersionName); }
           public void CreateBranch(string BranchName) { _Parent.CreateProjectBranch(_Name, BranchName); }
           public void AttachBranchToVersion(string VersionName, string BranchName) { _Parent.AttachBranchToVersion(_Name, VersionName, BranchName); }
           public void Submit(string VersionName, string BranchName, Fact.Processing.Project Project)
           {
               _Parent.Submit(_Name, VersionName, BranchName, Project);
           }
           public Submission[] GetSubmissions(string VersionName, string BranchName)
           {
               List<Submission> submissions = new List<Submission>();
               object result = _Parent._Client.Call("BuildfarmManager.GetSubmissionIds", _Name, VersionName, BranchName);
               if (result == null || !(result is string[]))
               {
                   throw new Exception("Transaction rejected by the server");
               }
               foreach (string id in (result as string[]))
               {
                   Submission s = new Submission();
                   s._ID = id;
                   s._Parent = _Parent;
                   submissions.Add(s);
               }
               return submissions.ToArray();
           }
           public Version[] Versions
           {
               get
               {
                   List<Version> versions = new List<Version>();
                   object result = _Parent._Client.Call("BuildfarmManager.GetProjectVersions", _Name);
                   if (result == null || !(result is string[]))
                   {
                       throw new Exception("Transaction rejected by the server");
                   }
                   foreach (string name in (result as string[]))
                   {
                       Version v = new Version();
                       v._Name = name;
                       v._Parent = _Parent;
                       v._Project = this;
                       versions.Add(v);
                   }
                   return versions.ToArray();
               }
           }
           public Branch[] Branchs
           {
               get
               {
                   List<Branch> branches = new List<Branch>();
                   object result = _Parent._Client.Call("BuildfarmManager.GetProjectBranches", _Name);
                   if (result == null || !(result is string[]))
                   {
                       throw new Exception("Transaction rejected by the server");
                   }
                   foreach (string name in (result as string[]))
                   {
                       Branch b = new Branch();
                       b._Name = name;
                       b._Parent = _Parent;
                       b._Project = this;
                       branches.Add(b);
                   }
                   return branches.ToArray();
               }
           }
       }

       public Buildfarm(Fact.Network.Client Client)
       {
            _Client = Client;
       }

       public void CreateProject(string Name)
       {
           Name = Name.Trim();
           if (Name == "") { throw new Exception("Invalid project name"); }
           object result = _Client.Call("BuildfarmManager.CreateProject", Name);
           if (result == null || !(result is bool) || !((bool)result))
           {
               throw new Exception("Transaction rejected by the server");
           }
       }
       public void Submit(string ProjectName, string VersionName, string BranchName, Fact.Processing.Project Project)
       {
           ProjectName = ProjectName.Trim();
           VersionName = VersionName.Trim();
           BranchName = BranchName.Trim();

           if (Project == null) { throw new Exception("Invalid project"); }
           if (ProjectName == "") { throw new Exception("Invalid project name"); }
           if (VersionName == "") { throw new Exception("Invalid version name"); }
           if (BranchName == "") { throw new Exception("Invalid branch name"); }


           object result = _Client.CallWithTimeout("BuildfarmManager.Submit", 60000, ProjectName, VersionName, BranchName, Project);
           if (result == null || !(result is bool) || !((bool)result))
           {
               throw new Exception("Transaction rejected by the server");
           }
       }
       public void CreateProjectVersion(string ProjectName, string VersionName)
       {
           ProjectName = ProjectName.Trim();
           VersionName = VersionName.Trim();
           if (ProjectName == "") { throw new Exception("Invalid project name"); }
           if (VersionName == "") { throw new Exception("Invalid version name"); }

           object result = _Client.Call("BuildfarmManager.CreateProjectVersion", ProjectName, VersionName);
           if (result == null || !(result is bool) || !((bool)result))
           {
               throw new Exception("Transaction rejected by the server");
           }
       }

       public void CreateProjectBranch(string ProjectName, string BranchName)
       {
           ProjectName = ProjectName.Trim();
           BranchName = BranchName.Trim();
           if (ProjectName == "") { throw new Exception("Invalid project name"); }
           if (BranchName == "") { throw new Exception("Invalid branch name"); }

           object result = _Client.Call("BuildfarmManager.CreateProjectBranch", ProjectName, BranchName);
           if (result == null || !(result is bool) || !((bool)result))
           {
               throw new Exception("Transaction rejected by the server");
           }
       }

       public void AttachBranchToVersion(string ProjectName, string VersionName, string BranchName)
       {
           ProjectName = ProjectName.Trim();
           VersionName = VersionName.Trim();
           BranchName = BranchName.Trim();

           if (ProjectName == "") { throw new Exception("Invalid project name"); }
           if (VersionName == "") { throw new Exception("Invalid version name"); }
           if (BranchName == "") { throw new Exception("Invalid branch name"); }


           object result = _Client.Call("BuildfarmManager.AttachBranchToVersion", ProjectName, VersionName, BranchName);
           if (result == null || !(result is bool) || !((bool)result))
           {
               throw new Exception("Transaction rejected by the server");
           }
       }

       public Project[] Projects
       {
           get
           {
               List<Project> projects = new List<Project>();
               object result = _Client.Call("BuildfarmManager.GetProjects");
               if (result == null || !(result is string[]))
               {
                   throw new Exception("Transaction rejected by the server");
               }
               foreach (string name in (result as string[]))
               {
                   Project p = new Project();
                   p._Name = name;
                   p._Parent = this;
                   projects.Add(p);
               }
               return projects.ToArray();
           }
       }
    }
}
